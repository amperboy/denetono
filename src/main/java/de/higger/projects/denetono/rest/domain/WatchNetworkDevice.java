package de.higger.projects.denetono.rest.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WatchNetworkDevice {

	@NotBlank(message = "Devicename is mandatory")
	@Pattern(regexp = "^[a-z][a-z0-9]{2,49}$")
	private String deviceName;

	@NotBlank(message = "Host is mandatory")
	private String host;

	@JsonProperty
	private boolean isReachableNotificationRequired;

	@JsonProperty
	private boolean isUnreachableNotificationRequired;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public boolean isReachableNotificationRequired() {
		return isReachableNotificationRequired;
	}

	public void setReachableNotificationRequired(boolean isReachableNotificationRequired) {
		this.isReachableNotificationRequired = isReachableNotificationRequired;
	}

	public boolean isUnreachableNotificationRequired() {
		return isUnreachableNotificationRequired;
	}

	public void setUnreachableNotificationRequired(boolean isUnreachableNotificationRequired) {
		this.isUnreachableNotificationRequired = isUnreachableNotificationRequired;
	}

}
