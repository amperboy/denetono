package de.higger.projects.denetono.process;

public class VariableNames {


	public static final String DEVICE_NAME = "deviceName";
	public static final String HOST = "host";
	public static final String REACHABLE_STATUS = "reachableStatus";
	public static final String LAST_STATUS = "lastStatus";
	public static final String IS_REACHABLE_NOTIFICATION_REQUIRED = "isReachableNotificationRequired";
	public static final String IS_UNREACHABLE_NOTIFICATION_REQUIRED = "isUnreachableNotificationRequired";
	
}
