package de.higger.projects.denetono.rest;

import java.util.Map;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MediaType;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.higger.projects.denetono.process.MessageNames;
import de.higger.projects.denetono.process.VariableNames;
import de.higger.projects.denetono.rest.domain.WatchNetworkDevice;

@RestController
@RequestMapping(path = "/rest/process")
public class ProcessRessource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRessource.class);

	@Inject
	private RuntimeService runtimeService;

	@PostMapping(consumes = { MediaType.APPLICATION_JSON })
	@Transactional
	public String startProcess(@Valid @RequestBody WatchNetworkDevice data) {

		String host = data.getHost();

		avoidMultipleInstancesForDevice(host);

		Map<String, Object> variables = Map.of(VariableNames.DEVICE_NAME, data.getDeviceName(), VariableNames.HOST,
				host, VariableNames.IS_REACHABLE_NOTIFICATION_REQUIRED,
				Boolean.valueOf(data.isReachableNotificationRequired()),
				VariableNames.IS_UNREACHABLE_NOTIFICATION_REQUIRED,
				Boolean.valueOf(data.isUnreachableNotificationRequired()));

		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("IsNetworkDeviceReachableProcess",
				data.getHost(), variables);

		String processInstanceId = processInstance.getProcessInstanceId();

		LOGGER.info("Started monitoring for host {}. Process runs with id {}.", host, processInstanceId);

		return processInstanceId;
	}

	@DeleteMapping(path = "/{host}")
	public void cancelProcess(@PathVariable String host) {
		runtimeService.correlateMessage(MessageNames.CANCEL_MONITORING, host);
		LOGGER.info("Monitoring for host {} cancelled.", host);
	}

	private void avoidMultipleInstancesForDevice(String host) {
		long runningInstances = runtimeService.createProcessInstanceQuery()
				.processInstanceBusinessKey(host)
				.active()
				.count();

		LOGGER.trace("Found {} running instances for host {}.", runningInstances, host);

		if (runningInstances > 0) {
			throw new BadRequestException("Device " + host + " already watched.");
		}
	}

}
