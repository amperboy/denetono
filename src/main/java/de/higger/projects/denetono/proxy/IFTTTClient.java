package de.higger.projects.denetono.proxy;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import de.higger.projects.denetono.proxy.domain.IFTTTEventData;

public interface IFTTTClient {

	@POST
	@Path("/trigger/{eventname}/with/key/{key}")
	@Consumes("application/json")
	void triggerEvent(@PathParam("eventname") String eventname, @PathParam("key") String key, IFTTTEventData data);
}
