package de.higger.projects.denetono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DenetonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DenetonoApplication.class, args);
	}

}
