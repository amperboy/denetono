package de.higger.projects.denetono.process.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

import de.higger.projects.denetono.process.VariableNames;

@Service
public class UpdateStatusDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		Boolean reachableStatusString = (Boolean) execution.getVariable(VariableNames.REACHABLE_STATUS);
		execution.setVariable(VariableNames.LAST_STATUS, reachableStatusString);
	}

}
