package de.higger.projects.denetono.proxy.domain;

public enum IFTTTEventType {

	DEVICE_OFFLINE("offline"), DEVICE_ONLINE("online");

	private String eventName;

	IFTTTEventType(String eventName) {
		this.eventName = eventName;
	}

	public String getEventPostfix() {
		return eventName;
	}

	public static IFTTTEventType fromMappedStatus(boolean mappedStatus) {
		if (mappedStatus) {
			return DEVICE_ONLINE;
		} else {
			return DEVICE_OFFLINE;
		}

	}

}
