package de.higger.projects.denetono;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("junit")
class DenetonoApplicationTests {

	@Test
	void contextLoads() {
	}

}