package de.higger.projects.denetono.proxy;

import javax.ws.rs.client.ClientBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class IFTTTClientProducer {

	@Bean
	public IFTTTClient createIFTTTClient(@Value("${service.ifttt.api}") String iftttServiceApi) {

		ResteasyClient client = (ResteasyClient) ClientBuilder.newClient();
		ResteasyWebTarget target = client.target(iftttServiceApi);

		return target.proxy(IFTTTClient.class);
	}
}
