# Hilfe

## IFTTT 'this' **anlegen**

* Zunächste auf ifttt.com anmelden und mit (1) ein neues Applet anlegen
 ![010_ifttt_create_action](./pages/help/010_ifttt_create_action.png)
* Im nächsten Schritt bei "If this" auf `add` drücken um einen Auslöser zu hinterlegen
 ![020_ifttt_create_this](./pages/help/020_ifttt_create_this.png)
* In das Suchfeld `webhooks` eintragen und den `Webhooks` Service auswählen
 ![030_search_webhooks_service](./pages/help/030_search_webhooks_service.png)
* Das `Receive a web request` Event als Auslöser wählen
 ![040_ifttt_select_web_request](./pages/help/040_ifttt_select_web_request.png)
* Eventname eintragen. Dieses setzt sich aus dem Gerätename (`deviceName`) und _offline (Gerät ist nicht mehr verfügbar) bzw. _online (Gerät ist jetzt verfügbar) zusammen. Beispiel "waschmaschine_offline" wenn das Gerät "waschmaschine" nicht mehr verfügbar ist.
 ![050_enter_event_name](./pages/help/050_enter_event_name.png)

## IFTTT Webhooks Key ermitteln

* Zunächste auf ifttt.com anmelden und über das eigene Avatar den `My services` Menueinträg auswählen.
 ![110_open_services](./pages/help/110_open_services.png)
* In der Liste den `Webhooks` Service wählen
 ![120_open_webhooks](./pages/help/120_open_webhooks.png)
* Die Webhooks Einstellungen (Settings) öffnen
 ![130_open_webhooks_settings](./pages/help/130_open_webhooks_settings.png)
* Der Key befindet sich am Ende der angezeigten URL (nach "https://maker.ifttt.com/use/"). Im Beispiel lautet der Key: `Jd83hd72DB_WJDnfdj83r`
 ![140_webhooks_key](./pages/help/140_webhooks_key.png) 
 
## Dockercontainer starten

Der folgende Befehl starten einen Dockercontainer mit der denetono App. **WICHTIG:** Die Variable `service.webhooks.key` muss mit dem eigenen ifttt-webhooks-Key überschrieben werden.

```
docker run -d -e spring.datasource.url=jdbc:oracle:thin:@127.0.0.1:1521:XE -e spring.datasource.username=camunda -e spring.datasource.password=camunda -e service.webhooks.key=Jd83hd72DB_WJDnfdj83r -p 9004:8080 -v "/volume1/docker/denetono:/data" amperboy/denetono:latest
```

### Überwachnung starten

Das Beispiel startet eine Überwachung für das Gerät "waschmaschine" (`deviceName`) welches unter 192.168.91.11 (`host`) erreichbar ist. Der Gerätename darf ausschließlich Kleinbuchstaben gefolgt von 49 Kleinbuchstaben oder Zahlen enthalten. Pattern: `^[a-z][a-z0-9]{2,49}$`. Mit `isReachableNotificationRequired` wird festgelegt ob bei Erreichbarkeit benachrichtigt werden soll. Mit `isUnreachableNotificationRequired` wird festgelegt ob bei Nichterreichbarkeit  benachrichtigt werden soll. 

```
curl --location --request POST 'http://localhost:9004/rest/process' \
--header 'Content-Type: application/json' \
--data-raw '{
    "deviceName": "waschmaschine",
    "host": "192.168.91.11",
    "isReachableNotificationRequired": false,
    "isUnreachableNotificationRequired": true
}'
```


### Überwachnung abbrechen

Das Beispiel beendet die Überwachung für das Gerät (`host`) welches unter `192.168.91.11` erreichbar ist.
```
curl --location --request DELETE 'http://localhost:8080/rest/process/192.168.91.11'
```
