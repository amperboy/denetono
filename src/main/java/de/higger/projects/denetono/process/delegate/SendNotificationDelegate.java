package de.higger.projects.denetono.process.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.higger.projects.denetono.process.VariableNames;
import de.higger.projects.denetono.proxy.IFTTTClient;
import de.higger.projects.denetono.proxy.domain.IFTTTEventData;
import de.higger.projects.denetono.proxy.domain.IFTTTEventType;

@Service
public class SendNotificationDelegate implements JavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendNotificationDelegate.class);

	@Autowired
	private IFTTTClient iftttClient;

	@Value("${service.webhooks.key}")
	private String iftttWebhookKey;

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		Boolean isReachable = (Boolean) execution.getVariable(VariableNames.REACHABLE_STATUS);

		String deviceName = (String) execution.getVariable(VariableNames.DEVICE_NAME);
		String hostname = (String) execution.getVariable(VariableNames.HOST);

		String eventPostfix = IFTTTEventType.fromMappedStatus(Boolean.valueOf(isReachable))
				.getEventPostfix();

		IFTTTEventData data = new IFTTTEventData();
		data.setValue1(deviceName.toString());
		data.setValue2(hostname.toString());

		iftttClient.triggerEvent(deviceName + "_" + eventPostfix, iftttWebhookKey, data);

		LOGGER.info("[{}] Sent notification for host {} with status {}.", execution.getProcessInstanceId(), hostname,
				isReachable);
	}

}
