package de.higger.projects.denetono.process.delegate;

import java.net.InetAddress;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.higger.projects.denetono.process.VariableNames;

@Service
public class GetReachableStatusDelegate implements JavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetReachableStatusDelegate.class);

	private static final int TIMEOUT_DEVICE_REACHABLE_CHECK = 1500;

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		String host = (String) execution.getVariable(VariableNames.HOST);
		InetAddress address = InetAddress.getByName(host);
		boolean isReachable = address.isReachable(TIMEOUT_DEVICE_REACHABLE_CHECK);
		LOGGER.debug("[{}] Host {} status is {}", execution.getProcessInstanceId(), host, isReachable);

		execution.setVariable(VariableNames.REACHABLE_STATUS, isReachable);
	}

}
