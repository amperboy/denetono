package de.higger.projects.denetono.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class Status {

	private static final Logger LOGGER = LoggerFactory.getLogger(Status.class);

	public boolean isReachableStatusChanged(DelegateExecution execution) {

		Boolean lastStatus = (Boolean) execution.getVariable(VariableNames.LAST_STATUS);
		if (lastStatus == null) {
			return true;
		}

		Boolean isReachable = (Boolean) execution.getVariable(VariableNames.REACHABLE_STATUS);

		boolean isReachableStatusChanged = lastStatus.booleanValue() != isReachable.booleanValue();
		LOGGER.trace("[{}] Last status: {}, current status: {}, status changed: {}", execution.getProcessInstanceId(), lastStatus, isReachable,
				isReachableStatusChanged);

		return isReachableStatusChanged;
	}

}
