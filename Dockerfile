FROM openjdk:16-alpine
ENV service.webhooks.key=IFTTT_WEBHOOKS_KEY
ENV spring.datasource.url=jdbc:oracle:thin:@localhost:1521:XE
ENV spring.datasource.username=camunda
ENV spring.datasource.password=camunda
ENV logging.level.de.higger.projects.denetono=INFO

RUN apk upgrade --update && apk add --no-cache tzdata freetype fontconfig ttf-dejavu
RUN  cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime

WORKDIR /application
COPY target/unzip/BOOT-INF/lib /application/lib
COPY target/unzip/META-INF /application/META-INF
COPY target/unzip/BOOT-INF/classes /application

CMD ["sh","-c","java -cp .:lib/* de.higger.projects.denetono.DenetonoApplication"]
